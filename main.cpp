#include <iostream>
#include <SFML/Graphics.hpp>

using namespace std;
using namespace sf;

int main()
{
    const int8_t CELL = 25;
    const float SPEED = 1 / 100.f;
    const uint16_t WIDTH = 500, HEIGHT = 500;

    RenderWindow window(VideoMode(WIDTH, HEIGHT), "Snake");

    RectangleShape Head;
    Head.setSize(Vector2f(CELL, CELL));
    Head.setFillColor(Color::Green);

    Vector2i Direction(1, 0);

    while(window.isOpen())
    {
        Event e;
        while(window.pollEvent(e))
            if(e.type == Event::Closed)
                window.close();

        Head.move(Direction.x * SPEED, Direction.y * SPEED);

        window.clear(Color::White);

        window.draw(Head);

        window.display();
    }
}
